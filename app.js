const express = require('express');
const exphbs = require('express-handlebars');
const bodyParser= require('body-parser');
const path = require('path');

const db = require('./config/database');
//Test DB
db.authenticate()
.then(()=>console.log('Database connected...'))
.catch(err=> console.log("Error: "+err))
const app = express();
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())
app.get('/',(req,res)=>{
    res.send('INDEX');
})

app.use('/categorylist', require('./routes/categoryRoutes'))

 
app.use(function (req, res) {
  res.setHeader('Content-Type', 'text/plain')
  res.write('you posted:\n')
  res.end(JSON.stringify(req.body, null, 2))
})

const PORT = process.env.PORT || 5000;

app.listen(PORT, console.log(`server started on port ${PORT}`))
