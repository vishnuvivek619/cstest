const express = require('express');
const router = express.Router();
const db = require('../config/database');
const category = require('../models/Categories')
const subcat = require('../models/subCategory')
const product = require('../models/product')
const async = require("async")
var bodyParser = require('body-parser')
var jsonParser = bodyParser.json()

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })
// router.get('/list', (req, res) =>
//     category.findAll().then(result => {
//         res.send(result);
//     })
//         .catch(err => console.log(err))
// );

router.post('/update', jsonParser, (req, res) => {
    category.findAll({
        where: {
            id: req.body.catId
        }
    }).then((Catresult) => {
        if (Catresult.length > 0) {
            let categoryUrl = createurl(req.body.categoryName, null, null)
            req.body.category_url = categoryUrl;
            category.update(re.body, {
                where: {
                    id: req.body.catId
                },
            }).then(((categoryAfterupadtes) => {
                if (categoryAfterupadtes.length > 0) {
                    console.log(categoryAfterupadtes)
                    subcat.findOne({
                        where: {
                            catNum: req.body.catId
                        }
                    }).then(subcategoryList => {
                        if (subcategoryList.length > 0) {
                           
                            subcategoryList.map((sublist) => {
                                let categoryUrl = createurl(req.body.categoryName, sublist.subCategoryName, null)
                                subcat.update({subcategory_url:categoryUrl}, {
                                    where: {
                                        catNum: req.body.catId
                                    }
                                }).then(subcategoryResults => {
                                    if (subcategoryResults.length > 0) {
                                        product.findOne({
                                            where: {
                                                sub_id: sublist.sub_category_code
                                            }
                                        }).then(productFind => {
                                            if (productFind.length > 0) {
                                                product.map((productResult) => {
                                                    let categoryUrl = createurl(req.body.categoryName, sublist.subCategoryName, productFind.product_name)
                                                    product.update({product_url:categoryUrl}, {
                                                        where: {
                                                            sub_id: productResult.product_id
                                                        }
                                                    }).then(productFinal => {
                                                        res.json({ status: "success", message: "Updated successfully", });
                                                    })
                                                })

                                            }
                                        })

                                    }
                                })



                            })
                        }
                    })
                } else {
                    res.json({
                        status: "failed",
                        message: "Document Not Found",
                    });
                }
            }))


        }

    }
    )
})


const createurl = (category, sub, product) => {
    let url = "";
    let domain = "ecommerse";
    if (category != null) {
        url = domain + "/" + category
    }
    if (sub != null) {
        url = url + "/" + sub;
    }
    if (product != null) {
        url = url + "/" + product
    }
    return url
}

module.exports = router;