const { Sequelize } = require('sequelize');
const sequelize = require('sequelize');
const db = require('../config/database');


const category = db.define('Category',{
    product_id: {
        type:Sequelize.INTEGER(11),
    },
    product_name: {
        type:Sequelize.STRING
    }

})
module.exports = category;