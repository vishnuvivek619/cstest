const { Sequelize } = require('sequelize');
const sequelize = require('sequelize');
const db = require('../config/database');


const category = db.define('Category',{
    sub_category_code: {
        type:Sequelize.INTEGER(11),
    },
    catNum: {
        type:Sequelize.INTEGER(11)
    }

})
module.exports = category;