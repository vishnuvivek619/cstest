const { Sequelize } = require('sequelize');
const sequelize = require('sequelize');
const db = require('../config/database');


const category = db.define('Categories',{
    categoryName: {
        type:Sequelize.STRING
    },
    id: {
        type:Sequelize.INTEGER(11),
        primaryKey:true
    }

})
module.exports = category;